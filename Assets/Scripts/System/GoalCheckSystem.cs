﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Burst;
using Unity.Collections;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class GoalCheckSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepsPhysicsWorld;
    private EntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate()
    {
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepsPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        entityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    private struct GoalCheckJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<GoalNetData> goalNetGroup;
        [ReadOnly] public ComponentDataFromEntity<BallData> ballGroup;
        public ComponentDataFromEntity<PhysicsVelocity> physicsVelocityGroup;
        public EntityCommandBuffer ecb;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity entityA = triggerEvent.Entities.EntityA;
            Entity entityB = triggerEvent.Entities.EntityB;

            bool isBodyATrigger = goalNetGroup.Exists(entityA);
            bool isBodyBTrigger = goalNetGroup.Exists(entityB);

            if (isBodyATrigger && isBodyBTrigger)
            {
                return;
            }

            bool isBodyADynamic = ballGroup.Exists(entityA);
            bool isBodyBDynamic = ballGroup.Exists(entityB);

            if ((isBodyATrigger && !isBodyBDynamic) || (isBodyBTrigger && !isBodyADynamic))
            {
                // Overlapping static bodies
                return;
            }

            var triggerEntity = isBodyATrigger ? entityA : entityB;
            var dynamicEntity = isBodyATrigger ? entityB : entityA;

            var triggerGoalNetComponent = goalNetGroup[triggerEntity];
            {
                GameManager.instance.PlayerScored(triggerGoalNetComponent.side);
            }
            {
                var component = physicsVelocityGroup[dynamicEntity];
                component.Linear *= 0;
                physicsVelocityGroup[dynamicEntity] = component;
            }

            ecb.DestroyEntity(dynamicEntity);
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var goalCheckJob = new GoalCheckJob
        {
            goalNetGroup = GetComponentDataFromEntity<GoalNetData>(true),
            ballGroup = GetComponentDataFromEntity<BallData>(true),
            physicsVelocityGroup = GetComponentDataFromEntity<PhysicsVelocity>(),
            ecb = entityCommandBufferSystem.CreateCommandBuffer()
        }.Schedule(stepsPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);

        entityCommandBufferSystem.AddJobHandleForProducer(goalCheckJob);

        //return default;
        return goalCheckJob;
    }
}
