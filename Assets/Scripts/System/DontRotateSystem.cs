﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(SimulationSystemGroup))]
public class DontRotateSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Entities.ForEach((ref PhysicsVelocity phys, ref Rotation rots, in DontRotateData dontRotateData) =>
        //Entities.ForEach((ref PhysicsVelocity phys, in DontRotateTag dontRotateTag) =>
        {
            if (dontRotateData.enabled)
            {
                phys.Angular *= 0;
                //rots.Value = quaternion.AxisAngle(dontRotateData.defaultRotation, 90);
                rots.Value = quaternion.RotateY(900);
            }
        }).Run();

        return default;
    }
}
