﻿using UnityEngine;
using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;

public class KeepBallOnYZero : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    { 
        Entities.ForEach((ref Translation trans, in BallData ballData) => 
        {
            trans.Value.y = 0;
        }).Run();

        return default;
    }
}
