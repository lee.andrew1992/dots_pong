﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class PlayerBumpAbilityInputSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    { 
        Entities.WithoutBurst().ForEach((ref PlayerBumpAbilityData bumpAbilityData, in PlayerInputData inputData, in Translation trans) => 
        {
            if (Input.GetKeyDown(inputData.testKeyBump))
            {
                if (bumpAbilityData.nextBump < UnityEngine.Time.time)
                {
                    bumpAbilityData.bumping = true;
                    bumpAbilityData.nextBump = UnityEngine.Time.time + bumpAbilityData.bumpCooldown;
                    bumpAbilityData.lastBump = UnityEngine.Time.time;
                }
            }

            if (bumpAbilityData.bumping && UnityEngine.Time.time > bumpAbilityData.lastBump + bumpAbilityData.bumpUpDuration)
            {
                bumpAbilityData.bumping = false;
            }
        }).Run();

        return default;
    }
}
