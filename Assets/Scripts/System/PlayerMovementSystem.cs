﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

[UpdateInGroup(typeof(SimulationSystemGroup))]
public class PlayerMovementSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;

        Entities.WithoutBurst().ForEach((ref Translation trans, in PlayerMovementData moveData) => 
        {
            trans.Value.y = 0;
            trans.Value.x = math.clamp(trans.Value.x - (moveData.speed * moveData.moveDirection * deltaTime), -GameManager.instance.xBound, GameManager.instance.xBound);
        }).Run();

        return default;
    }
}
