﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;

[AlwaysSynchronizeSystem]
public class IncreaseSpeedOverTimeSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;

        Entities.ForEach((ref PhysicsVelocity vel, in IncreaseSpeedOverTimeData data) =>
        {
            float2 modifier = new float2(data.increasePerSecond * deltaTime);
            float2 newVelocity = vel.Linear.xz;
            newVelocity = newVelocity + math.lerp(-modifier, modifier, math.sign(newVelocity));
            vel.Linear.xz = newVelocity;
        }).Run();

        return default;
    }
}
