﻿using UnityEngine;
using Unity.Jobs;
using Unity.Entities;

[AlwaysSynchronizeSystem]
public class PlayerInputSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Entities.ForEach((ref PlayerMovementData moveData, in PlayerInputData inputData) => 
        {
            moveData.moveDirection = 0;
            moveData.moveDirection += Input.GetKey(inputData.testKeyUp) ? 1 : 0;
            moveData.moveDirection -= Input.GetKey(inputData.testKeyDown) ? 1 : 0;
        }).Run();

        return default;
    }
}
