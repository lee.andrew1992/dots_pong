﻿using UnityEngine;
using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Physics;
using Unity.Mathematics;

public class PlayerBumpSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    { 
        float timeDelta = UnityEngine.Time.deltaTime;

        Entities.WithoutBurst().ForEach((ref Translation trans, in PlayerInputData inputData, in PlayerBumpAbilityData bumpAbilityData) => 
        {
            float targetZ = GameManager.instance.defaultPlayerZ * math.sign(trans.Value.z);
            if (bumpAbilityData.bumping)
            {
                targetZ = (GameManager.instance.defaultPlayerZ - bumpAbilityData.bumpRange) * math.sign(trans.Value.z);
            }
            
            trans.Value.z = math.lerp(trans.Value.z, targetZ, timeDelta * bumpAbilityData.bumpSpeed);
            
        }).Run();

        return default;
    }
}
