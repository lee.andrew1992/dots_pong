﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct GoalNetData : IComponentData
{
    public int side;
}
