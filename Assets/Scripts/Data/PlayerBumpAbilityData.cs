﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerBumpAbilityData : IComponentData
{
    public bool bumping;
    public float bumpRange;
    public float bumpSpeed;
    public float bumpUpDuration;
    public float bumpCooldown;
    public float nextBump;
    public float lastBump;
}
