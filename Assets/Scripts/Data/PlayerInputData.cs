﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerInputData : IComponentData
{
    public KeyCode testKeyUp;
    public KeyCode testKeyDown;
    public KeyCode testKeyBump;

    public float hZoneLimitMin;
    public float hZoneLimitMax;

    public float vZoneLimitMin;
    public float vZoneLImitMax;
}
