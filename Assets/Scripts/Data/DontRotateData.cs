﻿using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct DontRotateData : IComponentData
{
    public bool enabled;

    public float3 defaultRotation;
}
