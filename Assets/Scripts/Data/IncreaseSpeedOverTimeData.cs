﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct IncreaseSpeedOverTimeData : IComponentData
{
    public float increasePerSecond;
}
