﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerMovementData : IComponentData
{
    public int moveDirection;
    public float speed;
}
