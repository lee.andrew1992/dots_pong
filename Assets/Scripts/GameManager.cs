﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static GameManager instance { get { return _instance; } }
    private static GameManager _instance;

    public int[] goalCounter = new int [2] {0,0};
    public float xBound;
    public float ballBumpSpeedBonus;
    public float defaultPlayerZ;
    public float initialBallSpeed;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void PlayerScored(int side)
    {
        goalCounter[side] = goalCounter[side] + 1;
    }

    public void ResetScore()
    {
        goalCounter = new int[2] { 0, 0 };
    }
}
